# Sugarsyn wrapper #

This is just a copy of the https://pysugarsync.codeplex.com/ project. Plus some fixes for updates of sugarsync (authentification is now a little bit differenrt)

### How to instakk it ###
To install:
	python setup.py install

Required Dependancies:
	python 2.6+ 
	dateutil (tested on 2.1) - http://pypi.python.org/pypi/python-dateutil


### How to use it ###

=> Note that you must now have an application id in suggarsync. Must create it...

from pysugarsync import SugarSync
import ConfigParser

filename="conf.ini"
conf = ConfigParser.RawConfigParser()
conf.read(filename)
_sync = SugarSync(conf=conf)
syncFolders = _sync.ListSyncFolders()
files = _sync.ListFiles(syncFolders[0])
data = _sync.Read(files[0])


Configuration File Layout:

[SugarSync]
username=USER@name.com
password=PASS
apikey=YOUR-APIKEY
privatekey=YOUR-PRIVATEKEY
application=YOUR-APPLICATIONIF