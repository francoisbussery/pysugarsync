class SyncFile(object):
    """File class"""
    def __init__(self, name, ref, size, modified, dataurl):
        self._ident = {}
        self._ident["name"] = name
        self._ident["ref"] = ref
        self._ident["size"] = size
        self._ident["modified"] = modified
        self._ident["dataurl"] = dataurl

    def GetName(self):
        return self._ident["name"]

    def GetRef(self):
        return self._ident["ref"]

    def GetSize(self):
        return self._ident["size"]

    def GetModified(self):
        return self._ident["modified"]

    def GetDataUrl(self):
        return self._ident["dataurl"]

    def GetType(self):
        return "SyncFile"





