class SyncFolder(object):
    """Sync folder object"""
    def __init__(self, name, refurl, contentsurl):
        self._ident = {}
        self._ident["name"] = name
        self._ident["ref"] = refurl
        self._ident["contents"] = contentsurl

    def GetRefUrl(self):
        return self._ident["ref"]
    
    def GetName(self):
        return self._ident["name"]

    def GetContentUrl(self):
        return self._ident["contents"]

    def GetType(self):
        return "SyncFolder"

