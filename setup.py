from distutils.core import setup

setup(name = "pysugarsync",
    version = "0.2",
    description = "Third Party SugarSync API for Python",
    author = "welton",
    url = "https://pysugarsync.codeplex.com/",
    packages = ['pysugarsync']

    #This next part it for the Cheese Shop, look a little down the page.
    #classifiers = []     
) 